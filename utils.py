import os
import io

from pydub import AudioSegment

current_directory = os.getcwd()
print(current_directory)


def convert_wav_to_mp3(wav_bytes):
    filename = "temp.wav"
    mp3_file = "temp.mp3"

    with open("temp.wav", "wb") as file:
        file.write(wav_bytes)

    audio = AudioSegment.from_wav(os.path.join(current_directory, filename))
    audio.export(os.path.join("temp.mp3"), format="mp3")

    with open(os.path.join(current_directory, mp3_file), "rb") as file:
        mp3_bytes = file.read()

    os.remove("temp.wav")
    os.remove(mp3_file)
    return mp3_bytes


def generate_download_link(music_id: int, user_id: int):
    return f"http://127.0.0.1:8000/record?music_id={music_id}&user_id={user_id}"
