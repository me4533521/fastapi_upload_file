# fastapi_upload_file
## description
Project for test, script create user and upload file, add file to user.

## Getting started

Clone repository:
``` git clone https://gitlab.com/me4533521/fastapi_upload_file.git```

Create venv and install dependencies.

``` cd fastapi_upload_file```

```pip install -r requirements.txt ```

Start:

```source venv/bin/activate```

```python main.py```

## Using

POST request:
```http://127.0.0.1:8000/get_or_create_user/almat```

Get or create new user.

Returns:
```json {
    "id": 5,
    "token": "67fa92e1-9b01-45e3-982c-86e64c88a321",
    "name": "username",
    "musics": [
        {
            "id": 1,
            "token": "3987fd72-4d64-4d21-b176-1b8132fcb977",
            "name": "sample-15s"
        }
    ]
}
```
POST request:
```http://127.0.0.1:8000/music/upload```

Get or upload music file.

Returns:
```json
{
    "download_link": "http://127.0.0.1:8000/record?music_id=1&user_id=5"
}
```
Get request:
```http://127.0.0.1:8000/record?music_id=1&user_id=4```

Returns:
```Downlioad music file.```

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
