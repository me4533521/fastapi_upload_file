import logging
import uuid

from fastapi import HTTPException
from starlette import status

import models
import schemas


def get_or_create_user(name: str, db) -> schemas.UserSchemaRelation:
    try:
        db_user = db.query(models.User).filter(models.User.name.ilike(name)).first()
        if db_user:
            user = schemas.UserSchemaRelation(id=db_user.id,
                                              name=db_user.name,
                                              token=db_user.token,
                                              musics=db_user.musics)
            return user
        else:
            try:
                str_uuid = uuid.uuid4()
                new_user = models.User(token=str_uuid, name=name)
                db.add(new_user)
                db.commit()
                return schemas.UserSchemaRelation(id=new_user.id,
                                                  name=new_user.name,
                                                  token=new_user.token)
            except Exception as e:
                logging.exception(e)
                db.rollback()
    except Exception as e:
        logging.exception(e)
        raise e


def get_or_create_music(db, user_id, mp3_bytes, file_name):
    db_music = db.query(models.Music).filter(models.Music.file == mp3_bytes).first()
    db_user = db.query(models.User).filter(models.User.id == user_id).first()

    if db_music and db_user:
        if db_user not in db_music.users:
            db_music.users.append(db_user)
            db.commit()
        return db_music
    elif db_music and db_user is None:
        return db_music
    elif db_user and db_music is None:
        music = models.Music(file=mp3_bytes, name=file_name)
        music.users.append(db_user)
        db.add(music)
        db.commit()
        return music
    else:
        music = models.Music(file=mp3_bytes, name=file_name)
        db.add(music)
        db.commit()
        return music
