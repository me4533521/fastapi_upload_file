import logging

import uvicorn
from fastapi import FastAPI, Depends, HTTPException, UploadFile, File, Form, status, Response
from environs import Env
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

import crud, models, schemas
from database import engine, get_db
from utils import convert_wav_to_mp3, generate_download_link
from schemas import UserSchema

models.Base.metadata.create_all(bind=engine)

env = Env()
env.read_env()
app = FastAPI()

logging.basicConfig(
    format="%(levelname)s - %(message)s",
    level=logging.DEBUG,
    handlers=[
        logging.FileHandler("logfile.log"),
        logging.StreamHandler()
    ])

questions = []


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/get_or_create_user/{name}",
          status_code=status.HTTP_201_CREATED,
          response_model=schemas.UserSchema)
def get_or_create_user(name: str,
                       db: Session = Depends(get_db)):
    try:
        user = crud.get_or_create_user(name, db)
        return user
    except Exception as e:
        logging.exception(e)
        return JSONResponse(content={"message": "server error"},
                            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


@app.post("/music/upload")
def upload_music_file(user_id: int = Form(...),
                      user_token: str = Form(...),
                      file: UploadFile = File(...),
                      db: Session = Depends(get_db),
                      status_code=status.HTTP_201_CREATED,
                      response_model=dict
                      ):
    try:
        user = db.query(models.User).filter(models.User.id == user_id).first()
        if not user:
            return JSONResponse(content={"message": "user not found"},
                                status_code=status.HTTP_404_NOT_FOUND)
        if user.token != user_token:
            return JSONResponse(content={"message": "access denied"},
                                status_code=status.HTTP_403_FORBIDDEN)
        filename = file.filename.split(".")[0]
        wav_bytes = file.file.read()
        mp3_bytes = convert_wav_to_mp3(wav_bytes)
        music = crud.get_or_create_music(db, user_id, mp3_bytes, filename)
        download_link = generate_download_link(music.id, user_id)
        return {"download_link": download_link}
    except Exception as e:
        logging.exception(e)
        return JSONResponse(content={"message": "server error"},
                            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


@app.get("/record")
def download_record(music_id: int,
                    user_id: int,
                    response: Response,
                    db: Session = Depends(get_db),
                    response_model=str):
    try:
        if not db.query(models.User).filter(models.User.id == user_id).first():
            return JSONResponse(content={"message": "access denied"},
                                status_code=status.HTTP_403_FORBIDDEN)
        db_music = db.query(models.Music).filter(models.Music.id == music_id).first()
        if db_music:
            response.headers["Content-Disposition"] = f"attachment; filename={db_music.name}.mp3"
            response.headers["Content-Type"] = "audio/mpeg"
            response = Response(content=db_music.file,
                                media_type="audio/mpeg",
                                status_code=status.HTTP_200_OK)
            return response
        else:
            return JSONResponse(content={"message": "file not found"},
                                status_code=status.HTTP_404_NOT_FOUND)
    except Exception as e:
        logging.exception(e)
        return JSONResponse(content={"message": "server error"},
                            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)