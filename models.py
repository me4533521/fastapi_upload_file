import uuid
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, LargeBinary, Table
from sqlalchemy.orm import relationship
from database import Base


association_table = Table(
    "association",
    Base.metadata,
    Column("User_id", Integer, ForeignKey("users.id")),
    Column("Music_id", Integer, ForeignKey("musics.id")),
)


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    token = Column(String, default=str(uuid.uuid4()))
    name = Column(String(length=50))
    musics = relationship("Music", secondary=association_table, back_populates="users")


class Music(Base):
    __tablename__ = "musics"
    id = Column(Integer, primary_key=True)
    token = Column(String, default=str(uuid.uuid4()))
    file = Column(LargeBinary)
    name = Column(String(length=50))
    users = relationship("User", secondary=association_table, back_populates="musics")
