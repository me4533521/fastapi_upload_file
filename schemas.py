from pydantic import BaseModel, validator
from typing import List, Optional
from fastapi import UploadFile


class UserSchema(BaseModel):
    id: int
    token: str
    name: str
    musics: List["MusicSchema"] | List = []

    class Config:
        orm_mode = True


class UserSchemaRelation(BaseModel):
    id: int
    token: str
    name: str
    musics: List["MusicSchemaRelation"] | List = []

    @validator("musics")
    def validate_musics(cls, values):
        return [
            MusicSchemaRelation(id=x.id, token=x.token, name=x.name) for x in values
        ]


class MusicBase(BaseModel):
    token: str
    file: bytes
    name: str


class MusicCreate(MusicBase):
    pass


class MusicUpdate(MusicBase):
    pass


class MusicSchema(MusicBase):
    id: int
    users: List["UserSchema"] | List = []

    class Config:
        orm_mode = True


class MusicResponse(BaseModel):
    id: int
    token: str
    file: bytes


class MusicSchemaRelation(BaseModel):
    id: int
    token: str
    name: str


UserSchema.update_forward_refs()
# MusicSchema.update_forward_refs()
UserSchemaRelation.update_forward_refs()
# MusicSchemaRelation.update_forward_refs()
